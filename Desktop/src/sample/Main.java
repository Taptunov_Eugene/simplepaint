package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        stage.setScene(new Scene(FXMLLoader.load(getClass().
                getResource("sample.fxml"))));
        stage.setTitle("Simple Paint App");
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
