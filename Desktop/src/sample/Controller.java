package sample;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Controller {

    @FXML
    private Canvas canvas;

    @FXML
    private ColorPicker colorPicker;

    @FXML
    private Slider sliderSize;

    @FXML
    private Label label;

    @FXML
    private CheckBox eraser;

    @FXML
    private Button clear;

    public void initialize() {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();

        sliderSize.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                label.setText(String.format("%.0f", newValue));
                graphicsContext.setLineWidth(sliderSize.getValue());
            }
        });

        canvas.setOnMousePressed(event -> {
            if (eraser.isSelected()) {
                graphicsContext.clearRect(event.getX(), event.getY(), sliderSize.getValue(), sliderSize.getValue());
            } else {
                graphicsContext.beginPath();
                graphicsContext.lineTo(event.getX(), event.getY());
                graphicsContext.stroke();
            }
        });

        colorPicker.setOnAction(actionEvent -> {
            graphicsContext.setStroke(colorPicker.getValue());
        });

        canvas.setOnMouseDragged(event -> {
            sliderSize.setMin(1);
            sliderSize.setMax(50);

            if (eraser.isSelected()) {
                graphicsContext.clearRect(event.getX(), event.getY(), sliderSize.getValue(), sliderSize.getValue());
            } else {
                graphicsContext.lineTo(event.getX(), event.getY());
                graphicsContext.stroke();
                graphicsContext.setLineWidth(sliderSize.getValue());
                graphicsContext.setFill(colorPicker.getValue());
            }
        });
    }

    public void onSave() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png"),
                new FileChooser.ExtensionFilter("JPEG files (*.jpeg)", "*.jpeg"),
                new FileChooser.ExtensionFilter("BMP files (*.bmp)", "*.bmp"),
                new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.gif"));

        fileChooser.setInitialFileName("untitled");
        fileChooser.setInitialDirectory(new File("C:\\Users\\Jack\\" +
                "IdeaProjects\\SimplePaint\\src\\main\\resources"));
        File file = fileChooser.showSaveDialog(null);

        if (file != null) {
            try {
                WritableImage writableImage = canvas.snapshot(null, null);
                ImageIO.write(SwingFXUtils.fromFXImage(writableImage, null),"png", file);
            } catch (IOException e) {
                System.out.println("Failed to save image" + e);
            }
        }
    }

    public void onOpen(ActionEvent actionEvent) {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        FileChooser openFile = new FileChooser();
        openFile.setTitle("Open File");
        File file = openFile.showOpenDialog(null);
        if (file != null) {
            try {
                InputStream io = new FileInputStream(file);
                Image img = new Image(io);
                graphicsContext.drawImage(img, 0, 0);
            } catch (IOException ex) {
                System.out.println("Error!");
            }
        }
        GraphicsContext graphicsContext1;
        graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.setLineWidth(1);

    }

    public void onExit(ActionEvent actionEvent) {
        Platform.exit();
    }

    public void onClear(ActionEvent actionEvent) {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }
}
