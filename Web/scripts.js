var canvas;
var context;
var colorInput = document.getElementById("color");

window.onload = function() {
      canvas = document.getElementById("drawingCanvas");
      context = canvas.getContext("2d");
       
      canvas.onmousedown = startDrawing;
      canvas.onmouseup = stopDrawing;
      canvas.onmouseout = stopDrawing;
      canvas.onmousemove = draw;
}

var previousThicknessElement;
function changeThickness (thickness, imgElement) {
	context.lineWidth = thickness;
	imgElement.className = "Selected";
	if (previousThicknessElement != null)
	   previousThicknessElement.className = "";
	previousThicknessElement = imgElement;
}

function startDrawing(e) {
	isDrawing = true;
	context.beginPath();
	context.moveTo(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop);
}

function draw(event) {
	if (isDrawing == true) {
		var x = event.pageX - canvas.offsetLeft;
		var y = event.pageY - canvas.offsetTop;
		context.lineTo(x, y);
		context.stroke();
		context.strokeStyle = colorInput.value;
	}
}

function stopDrawing() {
    isDrawing = false;	
}

function clearCanvas() {
	context.clearRect(0, 0, canvas.width, canvas.height);
}

var url = canvas.toDataURL("image/jpeg");
window.location = canvas.toDataURL();
function saveCanvas() {
	var imageCopy = document.getElementById("savedImageCopy");
	imageCopy.src = canvas.toDataURL();
	var imageContainer = document.getElementById("savedCopyContainer");
    imageContainer.style.display = "block";
}